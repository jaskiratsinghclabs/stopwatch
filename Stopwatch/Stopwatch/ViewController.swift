//
//  ViewController.swift
//  Stopwatch
//
//  Created by Click Labs 65 on 1/16/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var secLabel: UILabel!
    
    @IBOutlet weak var millisecLabel: UILabel!
    
    
    @IBOutlet weak var minLabel: UILabel!
    
    //used to update time label
    
    @IBOutlet weak var splitTime: UILabel!
    
    //record split time
    
    var timer = NSTimer()
    
    
    //intialise the timer variable
    
    var count = 00 //to update time into this then into label
    
    var secCount = 00 // intialise second count
    
    var minCount = 00 // intialise minute count
    

    
    var flag = false // to ensure code is run one time on touch on play
    
    @IBAction func playButton(sender: AnyObject) {
        
        if flag == false {
        
        timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: "result", userInfo: nil, repeats: true)
    
            flag = true
            
        }else{
            
        }
    }
    
    
    //above function sets the timer to the interval of 1 second and after every 1 second it calls result function
    
    
    
    @IBAction func pauseButton(sender: AnyObject) {
        
        timer.invalidate()
        
        flag = false
        
        
    }
    
    //above function pauses the timer
    
    func result(){
        
        count++
        
        //below are the condition to update minute and second count
        
        if count % 100 == 0 {
            
            
            
            secCount++
            
            count = 0
            
            
            
        }
        
        
        
        if secCount % 60 == 0 && secCount != 0 {
            
            
            
            minCount++
            
            secCount = 0
            
            
            
        }
        
        if minCount == 60 {
        
            timer.invalidate()
        
        }
        
        

        
    millisecLabel.text = String(count)
        
        secLabel.text = String(secCount)
        
        minLabel.text = String(minCount)
        
        
        }
    
    //as playButton function call this at interval of one second it updates the label
    
    @IBAction func cancelButton(sender: AnyObject) {
        
        secLabel.text = "00"
        
        millisecLabel.text = "00"
        
        minLabel.text = "00"
        
        timer.invalidate()
        
        count = 00
        
        secCount = 00
        
        minCount = 00
        
        flag = false
        
        splitTime.text = ""
    }
    
    //above function resets the whole data
    
    
    @IBAction func splitDisp(sender: AnyObject) {
        
        if minCount == 0 && secCount == 0 && count == 0 {
            
        }else{
        
        splitTime.text = String(minCount)+" : "+String(secCount)+" : "+String(count)
        }
    }
    
    //split time updates split label
    
    
    @IBAction func LapDisp(sender: AnyObject) {
        
        splitTime.text = String(minCount)+" : "+String(secCount)+" : "+String(count)
        
       count = 0
        
        minCount = 0
        
        secCount = 0
        
    }
    
    // above function starts from the beginning and display the  previous time at splitlabel
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

